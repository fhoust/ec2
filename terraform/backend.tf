terraform {
    required_version = ">= 0.15.1"

    backend "s3" {
        bucket  = "terraform-tf-states"
        key     = "tfstates/ec2-server/ec2.tfstate"
        region  = "us-east-1"
        profile = "ec2-terraform"
    }
}