output "IP" {
  value       = aws_instance.ec2_server.public_ip
  description = "The public ip of intance"
}