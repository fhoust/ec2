#!/bin/bash
sudo usermod -aG docker ${USER}

sudo docker plugin install grafana/loki-docker-driver:latest --alias loki --grant-all-permissions
sudo docker network create loki

git clone https://fhoust@bitbucket.org/fhoust/metrics.git

cd metrics
sudo docker-compose up -d