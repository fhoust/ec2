variable "TAGS" {
  type        = map(string)
  default     = {
    Name      = "ec2-server"
    Component = "microblog"
    Location  = "AWS"
  }
}

variable "PROVIDER" {
  type         = map(string)
  default      = {
    Name       = "ec2-server"
    profile    = "ec2-terraform"
    aws_region = "us-east-1"
    path       = "$HOME/.aws/credentials"
  }
}

variable "KEY_PAIR_NAME" {
  default = "ec2_key"
}

variable "VPC" {
  default = "vpc-57c9fa2d"
}

variable "SUBNET"{
  default = "subnet-0bcf222a"
}