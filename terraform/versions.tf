terraform {
    required_version = ">= 0.15.1"

    required_providers {
      aws   = ">= 3.37.0"
      local = ">= 2.1.0"
      http  = ">= 2.1.0"
      tls   = ">= 3.1.0"
    }
}
