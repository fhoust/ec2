module "my_host" {
  source  = "jameswoolfenden/ip/http"
  version = "0.2.5"
}

resource "tls_private_key" "ec2_tls" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "ec2_key" {
  key_name   = var.KEY_PAIR_NAME
  public_key = tls_private_key.ec2_tls.public_key_openssh
}

resource "local_file" "key" {
  content         = tls_private_key.ec2_tls.private_key_pem
  filename        = "${path.module}/${aws_key_pair.ec2_key.key_name}.pem"
  file_permission = "0600"
}

resource "aws_security_group" "ec2_SG" {
  vpc_id      = var.VPC
  name        = "ec2_SG"
  description = "Access to EC2 Server"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "SSH for bitbucket"
  }

  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Blog for all world"
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${module.my_host.ip}/32"]
    description = "SSH from own IP"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    self        = true
  }

  tags = merge(var.TAGS)
}

resource "aws_instance" "ec2_server" {
  ami             = "ami-042e8287309f5df03"
  instance_type   = "t2.micro"
  key_name        = aws_key_pair.ec2_key.key_name
  security_groups = [aws_security_group.ec2_SG.id]
  subnet_id       = var.SUBNET
  tags            = merge(var.TAGS)

  connection {
    type        = "ssh"
    user        = "ubuntu"
    agent       = "false"
    private_key = tls_private_key.ec2_tls.private_key_pem
    host        = aws_instance.ec2_server.public_ip
  }

  provisioner "local-exec" {
    command = "echo '---\n    ip: ${aws_instance.ec2_server.private_ip}' > ../ansible/playbooks/ip.yaml"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ec2_server.public_ip} > hosts"
  }

  provisioner "local-exec" {
    command = "ssh-add ${var.KEY_PAIR_NAME}.pem"
  }

  provisioner "remote-exec" {
    script = "scripts/wait_boot.sh"
  }

  provisioner "local-exec" {
    command = "ansible-playbook ../ansible/playbooks/packages.yaml" 
  }

  provisioner "remote-exec" {
    script = "scripts/prepare_docker.sh"
  }
}