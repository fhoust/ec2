# EC2 instance with monitoring env

This project deploy a micro EC2 Ubuntu instance with a monitoring system.

## Why Ubuntu and micro tier

- Has free tier in AWS

## How to deploy

### Requirements

- [AWS cli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
- [Terraform](https://www.terraform.io/downloads.html)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
- [Metrics repo](https://bitbucket.org/fhoust/metrics/src/master/)

#### Add AWS Keys

Add you access_key and secret_access_key in `~/.aws/credentials`, if you do not have one, [click here](https://aws.amazon.com/pt/premiumsupport/knowledge-center/create-access-key/)

```
[ec2-terraform]
aws_access_key_id=AKIAIOSFODNN7EXAMPLE
aws_secret_access_key=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
```

#### Create a bucket

This project save all tfstates in a remote bucket. Check **terraform/backend.tf**
Bucket name: terraform-tf-states

#### VPC and Subnets

Create or use your default public VPC and Subnet ID of AWS to fill the variables in `terraform/variables/variables.tf`

```
...
variable "VPC" {
  default = "vpc-id"
}

variable "SUBNET"{
  default = "subnet-id"
}
```

## Running

In terraform folder, run:

``` bash
teraform init
terraform plan
terraform apply
```

## First access

When terraform finish, check the outputs for your IP:

- [Grafana](http://<your_ip>:3000)
- [Prometheus](http://<your_ip>:9090)
- [Loki Api](http://<your_ip>:3100)
- [Node_exporter](http://<your_ip>:9100)

### Grafana first access

In the first access to Grafana use: admin/admin

#### Creating datasources

To use dashboards you need to have datasources:

- Go to **Configuration** > **Datasources** > **Add data source**2.
    1. Prometheus: Add **http://prometheus:9090** in URL and save
    2. Loki: Add **http://grafana-loki:3100** in URL and save

#### Adding Dashboards

Look in **dashs** folders to get a list of ids and json templates for log and alert dashboards. [How export and import dashs](https://grafana.com/docs/grafana/latest/dashboards/export-import/)